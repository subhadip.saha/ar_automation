from CodeBase.mail_operations import *
from mapper import *
from invoice_generation import *
import pyodbc, sys

# Declaring Config path
varDict = {}
parentDir = os.path.dirname(os.getcwd())
configPath = rf"{parentDir}\Development\Configuration\Config.xlsx"

# Converting config variables into dictionary
varDF = pd.read_excel(configPath)
for index in varDF.index:
    varDict[varDF['Key'][index]] = varDF['Value'][index]

# Assigning Config variables to local variables
misRawDwnldFlPath = varDict['MIS Raw Download Destination Path']
arRawDwnldFlPath = varDict['AR Raw Download Destination Path']
InvoiceTmpltFlPath = varDict['Invoice Template Path']
opDocx = varDict['Word Invoice Path']
opPdf = varDict['PDF Invoice Path']
logFlPath = varDict['Log File Path']
moveMailFolder = varDict['Outlook folder to move processed mail']
arRawSrchStr = varDict['AR Input']
misRawSrchStr = varDict['MIS Input']

# Internal variables
misRawFlDirList = f"{misRawDwnldFlPath}\*.*"
arRawFlPath = f"{arRawDwnldFlPath}\Client_List.xlsx"
formatDataTmpltFlPath = rf"{parentDir}\Development\Input_Files\Template\Format_Data.xlsx"
invoiceNoMaintainerFlPath = rf"{parentDir}\Development\Input_Files\Invoice_No.csv"
formatDataOutPutPath = rf"{parentDir}\Development\Output_Files\Format_Data"
automationInputXlPath = fr"{parentDir}\Development\Output_Files\Format_Data\*.*"

# Removing previous files
for file in glob.glob(automationInputXlPath):
    os.remove(file)
for file in glob.glob(misRawFlDirList):
    os.remove(file)

# Downloading AR Raw Data and moving the Email
opDict = download_raw(arRawSrchStr, arRawDwnldFlPath)

mailIdLst = []
for i, status in enumerate(opDict["Execution_Status"]):
    if status == "Successful":
        mailIdLst.append(opDict["Message_ID"][i])

move_mail(arRawSrchStr, moveMailFolder, mailIdLst)

# Downloading MIS Raw Data and moving the Email
opDict = download_raw(misRawSrchStr, misRawDwnldFlPath)

mailIdLst = []
for i, status in enumerate(opDict["Execution_Status"]):
    if status == "Successful":
        mailIdLst.append(opDict["Message_ID"][i])

move_mail(misRawSrchStr, moveMailFolder, mailIdLst)
# --------------------------

# Mapping data
map_data(misRawFlDirList, arRawFlPath, formatDataTmpltFlPath, invoiceNoMaintainerFlPath, formatDataOutPutPath)

# Generating invoices
opDict = generate(InvoiceTmpltFlPath, automationInputXlPath, opDocx, opPdf, opDict)

# Logging
slNo = {"Sl_No": []}
for i in range(len(opDict["Date"])):
    slNo["Sl_No"] += [i + 1]

nwOpDict = {**slNo, **opDict}
mailDF = pd.DataFrame.from_dict(nwOpDict)

# Existing log last Sl No
curLogDF = pd.read_excel(logFlPath, sheet_name='MIS')
if curLogDF.shape[0] > 0:
    lastSlNo = curLogDF.iloc[-1]["Sl_No"]
else:
    lastSlNo = 0

# Correcting the Sl No
for index in mailDF.index:
    mailDF.at[index, 'Sl_No'] = index + 1 + lastSlNo

# Concatenating MailDF with UniqueFieldDF
uniqueFieldsDF = map_log(automationInputXlPath)
resultDF = pd.concat([mailDF, uniqueFieldsDF], axis=1)

# Logging to DB
# Connection details
server_name = '10.80.19.40'
database_name = 'mindrpa'
username = 'mindrpa'
password = 'cW^dug28!^Pw'

# Establish the database connection
connection_string = (
    f'DRIVER={{ODBC Driver 17 for SQL Server}};'
    f'SERVER={server_name};'
    f'DATABASE={database_name};'
    f'UID={username};'
    f'PWD={password};'
)

try:
    conn = pyodbc.connect(connection_string)
except Exception as e:
    print(f"Error: {str(e)}")
    sys.exit(1)  # Exit the script if there's an error with the database connection

# Generate the SQL query dynamically based on DataFrame columns
table_name = 'AR_Automation_Logs'
cursor = conn.cursor()
columns = ', '.join(resultDF.columns)
placeholders = ', '.join(['?' for _ in resultDF.columns])
sql_query = f"INSERT INTO {table_name} ({columns}) VALUES ({placeholders})"

try:
    # Iterate through DataFrame rows and insert them into the database
    for index, row in resultDF.iterrows():
        cursor.execute(sql_query, tuple(row))
        conn.commit()  # Commit the transaction for each row
except Exception as e:
    print(f"Error: {str(e)}")

# Close the connection when done
conn.close()

# Preparing the final DF
finalDF = pd.concat([curLogDF, resultDF], ignore_index=True)
finalDF.to_excel(logFlPath, sheet_name='MIS', index=False)

