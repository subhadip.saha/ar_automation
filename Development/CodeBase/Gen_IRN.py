import requests
from Gen_Token import *
import pandas as pd
import os
import json
from data import apiBody_static


# API endpoint URL
api_url = "https://gsp.adaequare.com/test/enriched/ei/api/invoice"
directory_path = "C:/Users/FKOL012330.Fusion/Downloads/AR_Automation/AR_Automation/Development/Output_Files/Format_Data/"
Client_List_Path = "C:/Users/FKOL012330.Fusion/Downloads/AR_Automation/AR_Automation/Development/Input_Files/AR_Input/Client_List.xlsx"

def Gen_irn():
    # Iterate through each file in the directory
    for filename in os.listdir(directory_path):
        if filename.endswith(".xlsx") or filename.endswith(".xls"):
            file_path = os.path.join(directory_path, filename)
            df = pd.read_excel(file_path)
            # Extract necessary data from the DataFrame df and populate the apiBody dictionary dynamically
            invoice_number = df['Value'].iloc[27]
            invoice_date = df['Value'].iloc[29]
            #SellerDtls
            seller_gstin = df['Value'].iloc[19]
            seller_legal_name = df['Value'].iloc[11]
            seller_address1 = df['Value'].iloc[18]
            seller_location = seller_address1[-12:]
            seller_pin = seller_address1[-6:]
            seller_state_code =seller_gstin[0:2]
            seller_email = df['Value'].iloc[16]
            #BuyerDtls
            buyer_gstin = df['Value'].iloc[3]
            buyer_legal_name = df['Value'].iloc[1]
            buyer_pos = df['Value'].iloc[5]
            buyer_address1 = df['Value'].iloc[2]
            buyer_location = buyer_address1[-12:]
            buyer_pin = "431701"
            buyer_state_code = df['Value'].iloc[5]
            buyer_email = df['Value'].iloc[9]
            #line Item 1
            item_slno1 = df['Value'].iloc[40]
            item_description1 = df['Value'].iloc[60]
            item_hsn_code = df['Value'].iloc[21]
            item_qty1 = df['Value'].iloc[70]
            item_unit_price1 = df['Value'].iloc[80]
            item_total_amount1 = df['Value'].iloc[90]
            item_ass_amt1 = df['Value'].iloc[90]
            item_gst_rate = df['Value'].iloc[31]
            item_igst_amt = item_ass_amt1 * (item_gst_rate/100)
            item_cgst_amt = df['Value'].iloc[32]
            item_sgst_amt = df['Value'].iloc[33]
            item_ces_rate = df['Value'].iloc[0]
            item_ces_amt = df['Value'].iloc[0]
            item_ces_non_advl_amt = df['Value'].iloc[0]
            item_state_ces_rate = df['Value'].iloc[0]
            item_state_ces_amt = df['Value'].iloc[0]
            item_state_ces_non_advl_amt = df['Value'].iloc[0]
            item_other_charges = df['Value'].iloc[0]
            item_total_item_value = item_total_amount1+item_igst_amt
            #line Item 2
            item_slno2 = df['Value'].iloc[41]
            item_description2 = df['Value'].iloc[61]
            item_hsn_code = df['Value'].iloc[21]
            item_qty2 = df['Value'].iloc[71]
            item_unit_price2 = df['Value'].iloc[81]
            item_total_amount2 = df['Value'].iloc[91]
            item_ass_amt2 = df['Value'].iloc[91]
            item_gst_rate = df['Value'].iloc[31]
            item_igst_amt1 = item_ass_amt2 * (item_gst_rate/100)
            item_cgst_amt = df['Value'].iloc[32]
            item_sgst_amt = df['Value'].iloc[33]
            item_ces_rate = df['Value'].iloc[0]
            item_ces_amt = df['Value'].iloc[0]
            item_ces_non_advl_amt = df['Value'].iloc[0]
            item_state_ces_rate = df['Value'].iloc[0]
            item_state_ces_amt = df['Value'].iloc[0]
            item_state_ces_non_advl_amt = df['Value'].iloc[0]
            item_other_charges = df['Value'].iloc[0]
            item_total_item_value1 = item_total_amount2+item_igst_amt1
            #line Item 3
            item_slno3 = df['Value'].iloc[42]
            item_description3 = df['Value'].iloc[62]
            item_hsn_code = df['Value'].iloc[21]
            item_qty3 = df['Value'].iloc[72]
            item_unit_price3 = df['Value'].iloc[82]
            item_total_amount3 = df['Value'].iloc[92]
            item_ass_amt3 = df['Value'].iloc[92]
            item_gst_rate = df['Value'].iloc[31]
            item_igst_amt2 = item_ass_amt3 * (item_gst_rate/100)
            item_cgst_amt = df['Value'].iloc[32]
            item_sgst_amt = df['Value'].iloc[33]
            item_ces_rate = df['Value'].iloc[0]
            item_ces_amt = df['Value'].iloc[0]
            item_ces_non_advl_amt = df['Value'].iloc[0]
            item_state_ces_rate = df['Value'].iloc[0]
            item_state_ces_amt = df['Value'].iloc[0]
            item_state_ces_non_advl_amt = df['Value'].iloc[0]
            item_other_charges = df['Value'].iloc[0]
            item_total_item_value2 = item_total_amount3+item_igst_amt2
            #line Item 4
            item_slno4 = df['Value'].iloc[43]
            item_description4 = df['Value'].iloc[63]
            item_hsn_code = df['Value'].iloc[21]
            item_qty4 = df['Value'].iloc[73]
            item_unit_price4 = df['Value'].iloc[83]
            item_total_amount4 = df['Value'].iloc[93]
            item_ass_amt4 = df['Value'].iloc[93]
            item_gst_rate = df['Value'].iloc[31]
            item_igst_amt3 = item_ass_amt4 * (item_gst_rate/100)
            item_cgst_amt = df['Value'].iloc[32]
            item_sgst_amt = df['Value'].iloc[33]
            item_ces_rate = df['Value'].iloc[0]
            item_ces_amt = df['Value'].iloc[0]
            item_ces_non_advl_amt = df['Value'].iloc[0]
            item_state_ces_rate = df['Value'].iloc[0]
            item_state_ces_amt = df['Value'].iloc[0]
            item_state_ces_non_advl_amt = df['Value'].iloc[0]
            item_other_charges = df['Value'].iloc[0]
            item_total_item_value3 = item_total_amount4+item_igst_amt3
            #line Item 5
            item_slno5 = df['Value'].iloc[44]
            item_description5 = df['Value'].iloc[64]
            item_hsn_code = df['Value'].iloc[21]
            item_qty5 = df['Value'].iloc[74]
            item_unit_price5 = df['Value'].iloc[84]
            item_total_amount5 = df['Value'].iloc[94]
            item_ass_amt5 = df['Value'].iloc[94]
            item_gst_rate = df['Value'].iloc[31]
            item_igst_amt4 = item_ass_amt5 * (item_gst_rate/100)
            item_cgst_amt = df['Value'].iloc[32]
            item_sgst_amt = df['Value'].iloc[33]
            item_ces_rate = df['Value'].iloc[0]
            item_ces_amt = df['Value'].iloc[0]
            item_ces_non_advl_amt = df['Value'].iloc[0]
            item_state_ces_rate = df['Value'].iloc[0]
            item_state_ces_amt = df['Value'].iloc[0]
            item_state_ces_non_advl_amt = df['Value'].iloc[0]
            item_other_charges = df['Value'].iloc[0]
            item_total_item_value4 = item_total_amount5+item_igst_amt4
            #val_ass_val = item_total_amount1
            #val_igst_val = item_igst_amt
            #val_tot_inv_val = val_ass_val+val_igst_val
            val_ass_val = item_total_amount1+item_total_amount2+item_total_amount3+item_total_amount4+item_total_amount5
            val_igst_val = item_igst_amt+item_igst_amt1+item_igst_amt2+item_igst_amt3+item_igst_amt4
            val_tot_inv_val = val_ass_val+val_igst_val
            apiBody = apiBody_static
            #DocDtls
            apiBody["DocDtls"]["Typ"] = "INV"
            apiBody["DocDtls"]["No"] = "XPL23wh764"
            apiBody["DocDtls"]["Dt"] = "01/01/2024"
            #SellerDtls
            apiBody["SellerDtls"]["Gstin"] = seller_gstin
            apiBody["SellerDtls"]["LglNm"] = seller_legal_name
            apiBody["SellerDtls"]["Addr1"] = seller_address1
            apiBody["SellerDtls"]["Loc"] = seller_location
            apiBody["SellerDtls"]["Pin"] = seller_pin
            apiBody["SellerDtls"]["Stcd"] = seller_state_code
            apiBody["SellerDtls"]["Em"] = seller_email
            #BuyerDtls
            apiBody["BuyerDtls"]["Gstin"] = buyer_gstin
            apiBody["BuyerDtls"]["LglNm"] = buyer_legal_name
            apiBody["BuyerDtls"]["Pos"] = buyer_pos
            apiBody["BuyerDtls"]["Addr1"] = buyer_address1
            apiBody["BuyerDtls"]["Loc"] = buyer_location
            apiBody["BuyerDtls"]["Pin"] = buyer_pin
            apiBody["BuyerDtls"]["Stcd"] = buyer_state_code
            apiBody["BuyerDtls"]["Em"] = buyer_email
            # Line Item 1
            apiBody["ItemList"][0]["SlNo"] = str(item_slno1)
            apiBody["ItemList"][0]["PrdDesc"] = item_description1
            apiBody["ItemList"][0]["HsnCd"] = str(item_hsn_code)
            apiBody["ItemList"][0]["Qty"] = item_qty1
            apiBody["ItemList"][0]["UnitPrice"] = item_unit_price1
            apiBody["ItemList"][0]["TotAmt"] = item_total_amount1
            apiBody["ItemList"][0]["AssAmt"] = item_ass_amt1
            apiBody["ItemList"][0]["GstRt"] = item_gst_rate
            apiBody["ItemList"][0]["IgstAmt"] = round(item_igst_amt,2)
            apiBody["ItemList"][0]["TotItemVal"] = round(item_total_item_value,2)
            # Line Item 2
            apiBody["ItemList"][1]["SlNo"] = str(item_slno2)
            apiBody["ItemList"][1]["PrdDesc"] = item_description2
            apiBody["ItemList"][1]["HsnCd"] = str(item_hsn_code)
            apiBody["ItemList"][1]["Qty"] = item_qty2
            apiBody["ItemList"][1]["UnitPrice"] = item_unit_price2
            apiBody["ItemList"][1]["TotAmt"] = item_total_amount2
            apiBody["ItemList"][1]["AssAmt"] = item_ass_amt2
            apiBody["ItemList"][1]["GstRt"] = item_gst_rate
            apiBody["ItemList"][1]["IgstAmt"] = round(item_igst_amt1,2)
            apiBody["ItemList"][1]["TotItemVal"] = round(item_total_item_value1,2)
            # Line Item 3
            apiBody["ItemList"][2]["SlNo"] = str(item_slno3)
            apiBody["ItemList"][2]["PrdDesc"] = item_description3
            apiBody["ItemList"][2]["HsnCd"] = str(item_hsn_code)
            apiBody["ItemList"][2]["Qty"] = item_qty3
            apiBody["ItemList"][2]["UnitPrice"] = item_unit_price3
            apiBody["ItemList"][2]["TotAmt"] = item_total_amount3
            apiBody["ItemList"][2]["AssAmt"] = item_ass_amt3
            apiBody["ItemList"][2]["GstRt"] = item_gst_rate
            apiBody["ItemList"][2]["IgstAmt"] = round(item_igst_amt2,2)
            apiBody["ItemList"][2]["TotItemVal"] = round(item_total_item_value2,2)
            # Line Item 4
            apiBody["ItemList"][3]["SlNo"] = str(item_slno4)
            apiBody["ItemList"][3]["PrdDesc"] = item_description4
            apiBody["ItemList"][3]["HsnCd"] = str(item_hsn_code)
            apiBody["ItemList"][3]["Qty"] = item_qty4
            apiBody["ItemList"][3]["UnitPrice"] = item_unit_price4
            apiBody["ItemList"][3]["TotAmt"] = item_total_amount4
            apiBody["ItemList"][3]["AssAmt"] = item_ass_amt4
            apiBody["ItemList"][3]["GstRt"] = item_gst_rate
            apiBody["ItemList"][3]["IgstAmt"] = round(item_igst_amt3,2)
            apiBody["ItemList"][3]["TotItemVal"] = round(item_total_item_value3,2)
            # Line Item 5
            apiBody["ItemList"][4]["SlNo"] = str(item_slno5)
            apiBody["ItemList"][4]["PrdDesc"] = item_description5
            apiBody["ItemList"][4]["HsnCd"] = str(item_hsn_code)
            apiBody["ItemList"][4]["Qty"] = item_qty5
            apiBody["ItemList"][4]["UnitPrice"] = item_unit_price5
            apiBody["ItemList"][4]["TotAmt"] = item_total_amount5
            apiBody["ItemList"][4]["AssAmt"] = item_ass_amt5
            apiBody["ItemList"][4]["GstRt"] = item_gst_rate
            apiBody["ItemList"][4]["IgstAmt"] = round(item_igst_amt4,2)
            apiBody["ItemList"][4]["TotItemVal"] = round(item_total_item_value4,2)
            #ValDtls
            apiBody["ValDtls"]["AssVal"] = round(val_ass_val,2)
            apiBody["ValDtls"]["IgstVal"] = round(val_igst_val,2)
            apiBody["ValDtls"]["TotInvVal"] = round(val_tot_inv_val,2)
            #convert the array into JSOM
            import json
            apiBodyJson = json.dumps(apiBody,indent=2)
            #print(apiBodyJson)
            # print(apiBody)
            # Headers
            headers = {
                "Content-Type": "application/json",
                "user_name": "xplor2024",
                "password": "Xplor@2024",
                "gstin": "19AAACX0323Q2Z0",
                "requestid": "536383",
                "Authorization": f"Bearer {access_token}"
            }
            # Make a POST request to the API
            response = requests.post(api_url, apiBodyJson, headers=headers)
            irn_number = ""
            if response.status_code == 200:
            # Parse and print the JSON response
                api_response = response.json()
                #print(api_response) 
                #irn_number=""
                irn_number = api_response.get('result', {}).get('Irn')
                if irn_number:
                    print(f"API Response for {filename}: Irn Number - {irn_number}")
                    new_value = irn_number
                    df.at[0, 'Value'] = new_value
                    output_excel_path = f"C:/Users/FKOL012330.Fusion/Downloads/AR_Automation/AR_Automation/Development/Output_Files/IRN_numberAdd/{filename}"
                    df.to_excel(output_excel_path, index=False)
                    print("done")
                else:
                    print(f"No 'Irn' number found in the API response for {filename}")
            elif 'message' in response and '!!!WARNING!!! This requestid is duplicate' in response['message']:
                # The warning message exists in the response
                print("!!!WARNING!!! This requestid is duplicate, returning previous response")
            else:
                print(f"Error for {filename}: {response.status_code} - {response.text}")




