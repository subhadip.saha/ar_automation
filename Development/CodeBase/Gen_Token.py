import requests
from requests.auth import HTTPBasicAuth


# Replace with your actual client ID and client secret
client_id = "079D114228B8430ABA792ACFDD639199"
client_secret = "993240D0G38CAG49E3G942BG50A319709C6E"
# Token endpoint URL
token_url = "https://gsp.adaequare.com/gsp/authenticate?grant_type=token"
# Define the data to be sent in the request body
data = {
    'grant_type': 'token',
}
# Make a POST request to the token endpoint with client ID and client secret
response = requests.post(token_url, auth=HTTPBasicAuth(client_id, client_secret), data=data)
# Check if the request was successful (status code 200)
if response.status_code == 200:
    # Parse the JSON response
    token_data = response.json()
    access_token = token_data.get('access_token')
    print(f"Access Token: {access_token}")
else:
    print(f"Error: {response.status_code} - {response.text}")