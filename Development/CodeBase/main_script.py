import pyodbc, sys
from mapper import *
from mail_module import *
from invoice_generation import *
from download_attachment import *
from Gen_IRN import *

# Declaring Config path
varDict = {}
parentDir = os.path.dirname(os.getcwd())
configPath = rf"{parentDir}\Development\Configuration\Config.xlsx"

# Converting config variables into dictionary
varDF = pd.read_excel(configPath)
for index in varDF.index:
    varDict[varDF['Key'][index]] = varDF['Value'][index]

# Assigning Config variables to local variables 
misRawDwnldFlPath = varDict['MIS Raw Download Destination Path']
arRawDwnldFlPath = varDict['AR Raw Download Destination Path']
InvoiceTmpltFlPath = varDict['Invoice Template Path']
opDocx = varDict['Word Invoice Path']
opPdf = varDict['PDF Invoice Path']
logFlPath = varDict['Log File Path']
moveMailFolder = varDict['Outlook folder to move processed mail']
arRawSrchStr = varDict['AR Input']
misRawSrchStr = varDict['MIS Input']

# Internal variables
misRawFlDirList = f"{misRawDwnldFlPath}\*.*"
arRawFlPath = f"{arRawDwnldFlPath}\Client_List.xlsx"
formatDataTmpltFlPath = rf"{parentDir}\Development\Input_Files\Template\Format_Data.xlsx"
invoiceNoMaintainerFlPath = rf"{parentDir}\Development\Input_Files\Invoice_No.csv"
formatDataOutPutPath = rf"{parentDir}\Development\Output_Files\Format_Data"
automationInputXlPath = fr"{parentDir}\Development\Output_Files\IRN_numberAdd\Format_Data*.*"

# Removing previous files
for file in glob.glob(automationInputXlPath):
    os.remove(file)
    print("File Deleted:", os.path.basename(file))
for file in glob.glob(misRawFlDirList):
    os.remove(file)
    print("File Deleted:", os.path.basename(file))

duplicate_email_move(arRawSrchStr)
duplicate_email_move(misRawSrchStr)

# Downloading AR Raw Data and moving the Email
opDict = download_raw(arRawSrchStr, arRawDwnldFlPath)

# Checking no of AR Mails
if len(opDict['Date']) > 0:
    mailIdLst = []
    for i, status in enumerate(opDict["Execution_Status"]):
        if status == "Successful":
            mailIdLst.append(opDict["Message_ID"][i])
    
    move_mail(arRawSrchStr, moveMailFolder, mailIdLst)

try:
    # Downloading MIS Raw Data and moving the Email
    opDict = download_raw(misRawSrchStr, misRawDwnldFlPath)
    
    # Checking no of MIS Mails
    if len(opDict['Date']) > 0:
        mailIdLst = []
        for i, status in enumerate(opDict["Execution_Status"]):
            if status == "Successful":
                mailIdLst.append(opDict["Message_ID"][i])

        move_mail(misRawSrchStr, moveMailFolder, mailIdLst)
    else:
        raise Exception("No MIS Mail Found.")
    
    # Mapping data
    map_data(misRawFlDirList, arRawFlPath, formatDataTmpltFlPath, invoiceNoMaintainerFlPath, formatDataOutPutPath)
    Gen_irn()
    # Generating invoices
    opDict = generate(InvoiceTmpltFlPath, automationInputXlPath, opDocx, opPdf, opDict)

    # Logging
    slNo = {"Sl_No": []}
    for i in range(len(opDict["Date"])):
        slNo["Sl_No"] += [i + 1]

    nwOpDict = {**slNo, **opDict}

    # Filling missing length with null
    for key, value in nwOpDict.items():
        max_len = max(map(len, nwOpDict.values()))
        nwOpDict[key] = value + [None] * (max_len - len(value))
    
    mailDF = pd.DataFrame.from_dict(nwOpDict)
    
    # Existing log last Sl No
    curLogDF = pd.read_excel(logFlPath, sheet_name='MIS')
    if curLogDF.shape[0] > 0:
        lastSlNo = curLogDF.iloc[-1]["Sl_No"]
    else:
        lastSlNo = 0

    # Correcting the Sl No
    for index in mailDF.index:
        mailDF.at[index, 'Sl_No'] = index + 1 + lastSlNo

    # Concatenating MailDF with UniqueFieldDF
    uniqueFieldsDF = map_log(automationInputXlPath, formatDataTmpltFlPath)

    resultDF = pd.concat([mailDF, uniqueFieldsDF], axis=1)

    resultDF = resultDF.astype(str)
    resultDF = resultDF.replace("nan", "-")
    
    # Logging to DB
    # Connection details
    server_name = '10.80.19.40'
    database_name = 'mindrpa'
    username = 'mindrpa'
    password = 'cW^dug28!^Pw'

    # Establish the database connection
    connection_string = (
        f'DRIVER={{ODBC Driver 17 for SQL Server}};'
        f'SERVER={server_name};'
        f'DATABASE={database_name};'
        f'UID={username};'
        f'PWD={password};'
    )
    
    try:
        conn = pyodbc.connect(connection_string)
    except Exception as e:
        print(f"Error: {str(e)}")
        sys.exit(1)  # Exit the script if there's an error with the database connection

    # Generate the SQL query dynamically based on DataFrame columns
    print("Creating Database Log...")
    table_name = 'AR_Automation_Logs'
    cursor = conn.cursor()
    columns = ', '.join(resultDF.columns)
    placeholders = ', '.join(['?' for _ in resultDF.columns])
    sql_query = f"INSERT INTO {table_name} ({columns}) VALUES ({placeholders})"
    
    try:
        # Iterate through DataFrame rows and insert them into the database
        for index, row in resultDF.iterrows():
            cursor.execute(sql_query, tuple(row))
            conn.commit()  # Commit the transaction for each row
    except Exception as e:
        print(f"Error: {str(e)}")
    
    # Close the connection when done
    conn.close()
    
    # Preparing the final DF
    print("Creating Local Log...")
    finalDF = pd.concat([curLogDF, resultDF], ignore_index=True)
    finalDF.to_excel(logFlPath, sheet_name='MIS', index=False)

except Exception as e:
    tz = pytz.timezone('Asia/Kolkata')
    dtToday = datetime.datetime.now(tz).strftime("%d-%b-%Y")
    tmToday = datetime.datetime.now(tz).strftime("%I:%M %p")
    str_sub = f"Alert!!! -- Error occured while generating invoices -- {dtToday} {tmToday}"
    str_from = "fusion.workforce@fusionbposervices.com"
    lst_to = ["nihar.rout@omindtech.com"]
    lst_cc = []
    lst_bcc = []
    msgbody = errMailCntnt(e)
    sendMail(str_sub, str_from, lst_to, lst_cc, lst_bcc, [], [], msgbody)
    

