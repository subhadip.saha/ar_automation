import os, glob
import xlwings as xw
from docxtpl import DocxTemplate
import win32com.client as win32
import datetime, pytz

def convert_to_pdf(doc, flNm):
    word = win32.DispatchEx("Word.Application")
    worddoc = word.Documents.Open(doc)
    worddoc.SaveAs(flNm, FileFormat=17)
    worddoc.Close()
    word.Quit()
    
    return None

def generate(InvoiceTmpltFlPath, automationInputXlPath, opDocx, opPdf, ipDict):
    # Time-Zone Variables
    tz = pytz.timezone('Asia/Kolkata')
    ipDict["Execution_End_Time"] = []

    # Format Data path
    app = xw.App(visible=False)
    if len(glob.glob(automationInputXlPath)) > 0:
        for file in glob.glob(automationInputXlPath):
            str_date = datetime.datetime.now(tz)
            
            wb = xw.Book(file)
            ws = wb.sheets['Sheet1']

            doc = DocxTemplate(InvoiceTmpltFlPath)
            context = ws.range('A2').options(dict, expand='table', numbers=float).value
            for j in context:
                if type(context[j]) == float:
                    if int(context[j]) == context[j] and isinstance(context[j], float):
                        context[j] = str(context[j])
                        context[j] = context[j].replace(".0", "")
                elif context[j] == None:
                    context[j] = ""
                elif type(context[j]) == datetime.datetime:
                    context[j] = context[j].strftime('%d-%m-%Y')

            doc.render(context)
            docNm = os.path.join(opDocx, f"Invoice_{str_date.strftime('%Y%m%d_%H%M%S')}.docx")
            doc.save(docNm)

            wb.close()

            print("PDF Generation:", f"Invoice_{str_date.strftime('%Y%m%d_%H%M%S')}")
            convert_to_pdf(docNm, os.path.join(opPdf, f"Invoice_{str_date.strftime('%Y%m%d_%H%M%S')}.pdf"))
            ipDict["Generated_Invoice_Name"] += [f"Invoice_{str_date.strftime('%Y%m%d_%H%M%S')}.pdf"]
            ipDict["Execution_End_Time"] += [datetime.datetime.now(tz).strftime("%H:%M:%S")]
    else:
        ipDict["Generated_Invoice_Name"] = ["-"]
        ipDict["Execution_End_Time"] = [datetime.datetime.now(tz).strftime("%H:%M:%S")]
    
    app.quit()

    return ipDict
