import smtplib
from os.path import basename
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

def sendMail(strSub, strFrom, listTo, listCc, listBcc, listImgPath, listAttachPath, msgBody):
    """
    strSub = The subject we need to pass while sending the mail.
    strFrom = From where the mail is being sent.
    listTo = List of recepients sepearted by comma in 'To' section
    listCc = List of recepients sepearted by comma in 'Cc' section
    listBcc = List of recepients sepearted by comma in 'Bcc' section
    listImgPath = List of images needs to be sent in the email, seperated by comma.
    listAttachPath = List of files which needs to be attached in the email, seperated by comma.
    listAttachFlNm = Attachment files name only, seperated by comma.
    msgBody = HTML message body which drafts the body of the mail.
    """
    # Create the root message 
    msgRoot = MIMEMultipart('related')
    msgRoot['Subject'] = strSub
    msgRoot['From'] = strFrom
    msgRoot['To'] = ', '.join(listTo)
    msgRoot['Cc'] = ', '.join(listCc)
    msgRoot['Bcc'] = ', '.join(listBcc)
    msgRoot.preamble = 'Multi-part message in MIME format.'

    msgAlternative = MIMEMultipart('alternative')
    msgRoot.attach(msgAlternative)

    msgText = MIMEText('Alternative plain text message.')
    msgAlternative.attach(msgText)

    msgText = MIMEText(msgBody, 'html')
    msgAlternative.attach(msgText)

    #Attach Image(s)
    if len(listImgPath) != 0:
        for index, item in enumerate(listImgPath):        
            fp = open(item, 'rb') #Read image 
            msgImage = MIMEImage(fp.read())
            fp.close()

            # Define the image's ID as referenced above
            msgImage.add_header('Content-ID', f'<image{index + 1}>')
            msgRoot.attach(msgImage)

    # Attach File
    if len(listAttachPath) != 0:
        for index, item in enumerate(listAttachPath):        
            with open(item, 'rb') as fil:
                part = MIMEApplication(fil.read(), basename(item))
            
            # Attaching the files with user defined name(s)
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(listAttachPath[index])
            msgRoot.attach(part)

    # Send the message via local SMTP server.
    s = smtplib.SMTP('smtp.office365.com', 587)
    s.set_debuglevel(1)
    s.ehlo()
    s.starttls()
    s.login(strFrom, 'Sun13Earth')
    s.sendmail(strFrom, (listTo + listCc + listBcc), msgRoot.as_string())
    s.quit()

def mailCntnt():
    mailBody = f"""
                <html>
                    <font size='2' face='Bookman Old Style' color='#3B3838'>
                    <body>
                        <p>
                            <br>
                                Hi Team,
                            <br>
                            <br>
                                This e-mail is auto-generated!
                            <br>
                            <br>
                                Your desired subject line.
                            <br>
                            <br>
                                Kind Regards,
                            <br>
                                Team Transformation
                        </p>
                    </body>
                </html>
            """
    return mailBody

def errMailCntnt(str_error):
    mailBody = f"""
                <html>
                    <font size='2' face='Bookman Old Style' color='#3B3838'>
                    <body>
                        <p>
                            <b>
                                Hi Team,
                            </b>
                            <br>
                            <br>
                                This e-mail is auto-generated.
                            <br>
                            <br>
                                <b>
                                    Alert!!!
                                </b>
                            <br>
                                Error occured while Generating Invoices.
                            <br>
                                Please find the error below:
                            <br>
                            <br>
                                {str_error}
                            <br>
                            <br>
                                Kind Regards,
                            <br>
                                Team Transformation
                        </p>
                    </body>
                </html>
            """
    return mailBody