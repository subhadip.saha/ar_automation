import win32com.client
import os, subprocess, time
import xml.etree.ElementTree as Et
from pywinauto.application import Application

# Declaring Config path
parentDir = os.getcwd()
configPath = rf"{parentDir}\Configuration\Config.csv"
invoiceAddRqXmlPath = rf"{parentDir}\XMP_Request\invoiceAddRq.xml"

def qb_conn():
    sessionManager = win32com.client.Dispatch("QBXMLRP2.RequestProcessor")
    sessionManager.OpenConnection('', 'SingleUser')
    ticket = sessionManager.BeginSession('', 0)
    
    return (sessionManager, ticket)

import csv
data_dict = {}
with open(configPath, 'r') as csv_file:
    reader = csv.reader(csv_file)
    for row in reader:
        if len(row) > 0:
            key, value = row[0], row[1]
            data_dict[key] = value

subprocess.run(f"{data_dict['QB_App_Path']} {data_dict['QB_FL_Path']}")

# Targeting QuickBooks window and cicking on MakeChanges button
app = Application(backend='win32').connect(title='Internet Security Levels Are Set Too High', timeout=30)
app.window(title='Internet Security Levels Are Set Too High').set_focus()
mkChangesBtn = app.InternetSecurityLevelsAreSetTooHigh.child_window(title="Cancel", class_name="MauiPushButton").wrapper_object()

time.sleep(1)
mkChangesBtn.click_input()

# Clicking on Yes Button
yesBtn = app.InternetSecurityLevelsConfiguration.child_window(title="&Yes", class_name="MauiPushButton").wrapper_object()

time.sleep(1)
yesBtn.click_input()

# Typing the password
passwordBtn = app.QuickBooksDesktopLogin.child_window(class_name="Edit", found_index=0).wrapper_object()

time.sleep(1)
passwordBtn.type_keys("Niraj@2021")

# Clicking on Ok button
okBtn = app.QuickBooksDesktopLogin.child_window(title="OK", class_name="MauiPushButton").wrapper_object()

time.sleep(1)
okBtn.click_input()
time.sleep(10)

# Connecting to the QuickBooks
sessionManager, ticket = qb_conn()

# Reading the XML query
with open(invoiceAddRqXmlPath, 'r') as f:
    xml_query = f.read()

# Storing query response
response = sessionManager.ProcessRequest(ticket, xml_query)

# Writing the response
with open("output.xml", "w") as f:
    f.write(response)

# Ending QuickBooks session
sessionManager.EndSession(ticket)

print("Invoice Raised Successfully...")