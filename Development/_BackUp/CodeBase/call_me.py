from download_attachment import *
from mapper import *
from invoice_generation import *

# Declaring Config path
varDict = {}
parentDir = os.path.dirname(os.getcwd())
configPath = rf"{parentDir}\Development\Configuration\Config.xlsx"

# Converting config variables into dictionary
varDF = pd.read_excel(configPath)
for index in varDF.index:
    varDict[varDF['Key'][index]] = varDF['Value'][index]

# Assigning Config variables to local variables
misRawDwnldFlPath = varDict['MIS Raw Download Destination Path']
arRawDwnldFlPath = varDict['AR Raw Download Destination Path']
InvoiceTmpltFlPath = varDict['Invoice Template Path']
opDocx = varDict['Word Invoice Path']
opPdf = varDict['PDF Invoice Path']
logFlPath = varDict['Log File Path']
moveMailFolder = varDict['Outlook folder to move processed mail']
arRawSrchStr = varDict['AR Input']
misRawSrchStr = varDict['MIS Input']

# Internal variables
misRawFlDirList = f"{misRawDwnldFlPath}\*.*"
arRawFlPath = f"{arRawDwnldFlPath}\Client_List.xlsx"
formatDataTmpltFlPath = rf"{parentDir}\Development\Input_Files\Template\Format_Data.xlsx"
invoiceNoMaintainerFlPath = rf"{parentDir}\Development\Input_Files\Invoice_No.csv"
formatDataOutPutPath = rf"{parentDir}\Development\Output_Files\Format_Data"
automationInputXlPath = fr"{parentDir}\Development\Output_Files\Format_Data\*.*"

# Removing previous files
for file in glob.glob(automationInputXlPath):
    os.remove(file)
for file in glob.glob(misRawFlDirList):
    os.remove(file)
# ---------------------------

# Downloading AR Raw Data and moving the Email
opDict = download_raw(arRawSrchStr, arRawDwnldFlPath)

mailIdLst = []
for i, status in enumerate(opDict["Execution Status"]):
    if status == "Successful":
        mailIdLst.append(opDict["Message ID"][i])

move_mail(arRawSrchStr, moveMailFolder, mailIdLst)
# ---------------------------

# Downloading MIS Raw Data and moving the Email
opDict = download_raw(misRawSrchStr, misRawDwnldFlPath)

mailIdLst = []
for i, status in enumerate(opDict["Execution Status"]):
    if status == "Successful":
        mailIdLst.append(opDict["Message ID"][i])

move_mail(misRawSrchStr, moveMailFolder, mailIdLst)
# --------------------------

# Mapping data
map_data(misRawFlDirList, arRawFlPath, formatDataTmpltFlPath, invoiceNoMaintainerFlPath, formatDataOutPutPath)

# Generating invoices
opDict = generate(InvoiceTmpltFlPath, automationInputXlPath, opDocx, opPdf, opDict)

# Logging
slNo = {"Sl. No": []}
for i in range(len(opDict["Date"])):
    slNo["Sl. No"] += [i + 1]

nwOpDict = {**slNo, **opDict}
mailDF = pd.DataFrame.from_dict(nwOpDict)

# Existing log last Sl No
curLogDF = pd.read_excel(logFlPath, sheet_name='MIS')
if curLogDF.shape[0] > 0:
    lastSlNo = curLogDF.iloc[-1]["Sl. No"]
else:
    lastSlNo = 0

# Correcting the Sl No
for index in mailDF.index:
    mailDF.at[index, 'Sl. No'] = index + 1 + lastSlNo

# Concatenating MailDF with UniqueFieldDF
uniqueFieldsDF = map_log(automationInputXlPath)
resultDF = pd.concat([mailDF, uniqueFieldsDF], axis=1)

# Preparing the final DF
finalDF = pd.concat([curLogDF, resultDF], ignore_index=True)
finalDF.to_excel(logFlPath, sheet_name='MIS', index=False)

