import imaplib
import os, msal
import email
import pytz
import datetime

def conn():
    global imap
    emailID = 'fusion.workforce@fusionbposervices.com'
    server = "outlook.office365.com"
    clientSecret = "5Ae8Q~8JZlJTjJpLfVhbkk-wPRP.6-Q1I1cpnbHl"
    clientID = "734f73b4-06ed-4a68-887d-ea493e10da44"
    tenantID = 'd3a38305-d78d-402b-a3db-8f598d615ae1'

    imap = imaplib.IMAP4_SSL(server, 993)
    imap.debug = 4
    access_token = get_access_token(tenantID,clientID,clientSecret)
    imap.authenticate("XOAUTH2", lambda x:generate_auth_string(emailID,access_token['access_token']))
    return imap

def get_access_token(tenantID, clientID, clientSecret):
    authority = 'https://login.microsoftonline.com/' + tenantID
    scope = ['https://outlook.office365.com/.default']
    app = msal.ConfidentialClientApplication(clientID, 
            authority=authority, 
            client_credential = clientSecret)
    access_token = app.acquire_token_for_client(scopes=scope)
    return access_token

def generate_auth_string(user, token):
    auth_string = f"user={user}\x01auth=Bearer {token}\x01\x01"
    return auth_string

def download_raw(subject_to_search, attachment_save_path):
    tz = pytz.timezone('Asia/Kolkata')
    try:
        # Creating Log Dictionary
        logDict = {
                    "Date": [],
                    "Sender": [],
                    "Subject": [],
                    "Mail Receiving Time": [],
                    "Has XL Attachment": [],
                    "Attachment Name": [],
                    "Message ID": [],
                    "Execution Start Time": [],
                    "Execution End Time": [],
                    "Execution Status": [],
                    "Reason": []
                    }

        # Connect to the IMAP server
        mail = conn()

        # Select the mailbox you want to search in (e.g., 'inbox')
        mailbox = 'INBOX'
        mail.select(mailbox)

        # Search for emails with the specific subject
        search_criteria = f'SUBJECT "{subject_to_search}"'
        status, email_ids = mail.search(None, search_criteria)

        if status == 'OK':
            email_id_list = email_ids[0].split()
            for email_id in email_id_list:

                # Logging start time & date
                logDict["Date"] += [datetime.datetime.now(tz).strftime("%d-%m-%Y")]
                logDict["Execution Start Time"] += [datetime.datetime.now(tz).strftime("%H:%M:%S")]

                # Fetch the email by UID
                status, email_data = mail.fetch(email_id, '(UID BODY.PEEK[])')

                if status == 'OK':
                    email_message = email.message_from_bytes(email_data[0][1])
                    _, sender_email = email.utils.parseaddr(email_message['From'])
                    email_received_time = email_message['Date']

                    # Parse the email_received_time into a datetime object
                    email_received_time = datetime.datetime.strptime(email_received_time, "%a, %d %b %Y %H:%M:%S %z")
                    
                    # Convert the time to the target time zone (IST)
                    email_received_time = email_received_time.astimezone(tz)

                    # Format the time as "dd/mm/yyyy hh:mm:ss"
                    email_received_time = email_received_time.strftime("%d-%m-%Y %H:%M:%S")

                    email_subject = email_message['Subject']
                    logDict["Sender"] += [sender_email]
                    logDict["Subject"] += [email_subject]
                    logDict["Mail Receiving Time"] += [email_received_time]
                    logDict["Message ID"] += [email_id.decode('utf-8')]

                    # Check for attachments
                    for part in email_message.walk():
                        if part.get_content_maintype() == 'multipart':
                            continue
                        if part.get('Content-Disposition') is None:
                            continue

                        # Save the attachment
                        filename = part.get_filename()

                        if not "image001.png" in filename:
                            if '.xlsx' in filename:
                                print("Downloading File:", filename)

                                attachment_path = os.path.join(attachment_save_path, filename)
                                with open(attachment_path, 'wb') as f:
                                    f.write(part.get_payload(decode=True))

                                print(f"Attachment '{filename}' saved to {attachment_path}")
                                logDict["Has XL Attachment"] += ["Yes"]
                                logDict["Execution End Time"] += [datetime.datetime.now(tz).strftime("%H:%M:%S")]
                                logDict["Execution Status"] += ["Successful"]
                                logDict["Reason"] += ["-"]
                            else:
                                logDict["Has XL Attachment"] += ["No"]
                                print("Ignoring File:", filename)
                            
                            logDict["Attachment Name"] += [filename]

                    # Return the UID of the email
                    print(f"UID of email with '{subject_to_search}': {email_id.decode('utf-8')}")
        
    except Exception as e:
        # Error Handling
        def fillup(keyNm, switch):
            if len(logDict[keyNm]) < keyLen:
                for i in range(keyLen - len(logDict[keyNm])):
                    if switch == 0:
                        logDict[keyNm] += ["-"]
                    else:
                        logDict[keyNm] += ["Failed"]
        
        keyLen = len(logDict["Date"])
        if keyLen > 0:

            # Filling Blank values in the dictionary
            fillup("Sender", 0)
            fillup("Subject", 0)
            fillup("Mail Receiving Time", 0)
            fillup("Has XL Attachment", 0)
            fillup("Attachment Name", 0)
            fillup("Message ID", 0)
            fillup("Execution Start Time", 0)
            fillup("Execution End Time", 0)
            fillup("Execution Status", 1)
            
            for i in range(keyLen - len(logDict["Reason"])):
                logDict["Reason"] += [e] 
    finally:
        try:
            # Logout and close the connection
            mail.logout()
        except Exception:
            pass
        return logDict

def move_mail(subject_to_search, move_mail_folder, email_id_list):
    # Connect to the IMAP server
    mail = conn()

    # Select the mailbox you want to search in (e.g., 'inbox')
    mailbox = 'INBOX'
    mail.select(mailbox)

    # Fetch the UIDs for all matching emails
    uids = []

    if len(email_id_list) > 0:
        for email_id in email_id_list:
            result, data = mail.fetch(email_id, "(UID)")
            if result == "OK":
                uid = data[0].split()[-1].decode("utf-8").replace(")", "")
                uids.append(uid)
    else:
        print("No emails found! with Subject --", subject_to_search)

    # Print or process the UIDs
    for uid in uids:
        mail.uid("MOVE", uid, move_mail_folder)
        
    # Logout and close the connection
    mail.logout()
    