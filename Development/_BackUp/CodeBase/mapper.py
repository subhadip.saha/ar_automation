import glob
import pandas as pd
import pytz, datetime
import csv


def map_data(misRawFlDirList, arRawFlPath, formatDataTmpltFlPath, invoiceNoMaintainerFlPath, formatDataOutPutPath):
    arRawDF = pd.read_excel(arRawFlPath)
    tz = pytz.timezone('Asia/Kolkata')

    i = 1
    for flDir in glob.glob(misRawFlDirList):
        tmpltDF = pd.read_excel(formatDataTmpltFlPath, keep_default_na=False)
        
        str_date = datetime.datetime.now(tz)
        invoiceDt = str_date.strftime("%d-%m-%Y")

        misRawDF = pd.read_excel(flDir, sheet_name="BOT_Input")
        startDate = misRawDF.at[0,'Start Date'].to_pydatetime()
        endDate = misRawDF.at[0,'End Date'].to_pydatetime()
        serviceMonth = startDate.strftime("%B %Y")
        amtBforeTax = misRawDF['Amount'].sum(axis=0)

        # Invoice Fillup 
        for index in arRawDF.index:
            if arRawDF['Client Name'][index] == misRawDF['Client'][0]:
                if arRawDF['Client Location'][index] == misRawDF['Location'][0]:
                    if arRawDF['Category'][index] == misRawDF['Category'][0]:

                        # GST Logic
                        CGST_Amt = ""
                        SGST_Amt = ""
                        IGST_Amt = ""

                        if int(arRawDF['Client State Code'][index]) == int(arRawDF['Supplier State Code'][index]):
                            CGST_Amt = round(amtBforeTax * (int(arRawDF['CGST'][index])/100), 2)
                            SGST_Amt = round(amtBforeTax * (int(arRawDF['SGST'][index])/100), 2)
                            amtBforeTax = round(amtBforeTax, 2)
                            totalAmount = round(amtBforeTax + SGST_Amt + CGST_Amt, 2)
                        else:
                            IGST_Amt = round(amtBforeTax * (int(arRawDF['IGST'][index])/100), 2)
                            amtBforeTax = round(amtBforeTax, 2)
                            totalAmount = round(amtBforeTax + IGST_Amt, 2)

                        tmpltDF.at[0, 'Value'] = arRawDF['IRN No'][index]
                        tmpltDF.at[1, 'Value'] = arRawDF['Client Name'][index]
                        tmpltDF.at[2, 'Value'] = arRawDF['Client Address'][index]
                        tmpltDF.at[3, 'Value'] = arRawDF['Client GSTIN'][index]
                        tmpltDF.at[4, 'Value'] = arRawDF['Client PAN'][index]
                        tmpltDF.at[5, 'Value'] = arRawDF['Client State Code'][index]
                        tmpltDF.at[6, 'Value'] = arRawDF['RCM Clause'][index]
                        tmpltDF.at[7, 'Value'] = arRawDF['Exempted Services'][index]
                        tmpltDF.at[8, 'Value'] = arRawDF['Client Contact Person'][index]
                        tmpltDF.at[9, 'Value'] = arRawDF['Contact Person Email ID'][index]
                        tmpltDF.at[10, 'Value'] = arRawDF['Employee Code'][index]
                        tmpltDF.at[11, 'Value'] = arRawDF['Supplier Brand Name'][index]
                        tmpltDF.at[12, 'Value'] = arRawDF['Supplier Mother Name'][index]
                        tmpltDF.at[13, 'Value'] = arRawDF['Supplier CIN'][index]
                        tmpltDF.at[14, 'Value'] = arRawDF['Supplier Udhyog Adhaar No'][index]
                        tmpltDF.at[15, 'Value'] = arRawDF['Supplier Website'][index]
                        tmpltDF.at[16, 'Value'] = arRawDF['Supplier Email ID'][index]
                        tmpltDF.at[17, 'Value'] = arRawDF['Supplier Phone No'][index]
                        tmpltDF.at[18, 'Value'] = arRawDF['Supplier Registered Office Address'][index]
                        tmpltDF.at[19, 'Value'] = arRawDF['Supplier GSTIN'][index]
                        tmpltDF.at[20, 'Value'] = arRawDF['Supplier PAN'][index]
                        tmpltDF.at[21, 'Value'] = arRawDF['SAC'][index]
                        tmpltDF.at[22, 'Value'] = arRawDF['Supplier State Code'][index]
                        tmpltDF.at[23, 'Value'] = arRawDF['Vendor Code'][index]
                        tmpltDF.at[24, 'Value'] = arRawDF['PO Number'][index]
                        tmpltDF.at[25, 'Value'] = arRawDF['Payment Terms'][index]
                        tmpltDF.at[26, 'Value'] = arRawDF['Place of Service'][index]
                        
                        tmpltDF.at[28, 'Value'] = serviceMonth
                        tmpltDF.at[29, 'Value'] = invoiceDt
                        tmpltDF.at[30, 'Value'] = amtBforeTax
                        tmpltDF.at[31, 'Value'] = arRawDF['IGST'][index]
                        tmpltDF.at[32, 'Value'] = arRawDF['CGST'][index]
                        tmpltDF.at[33, 'Value'] = arRawDF['SGST'][index]
                        tmpltDF.at[34, 'Value'] = IGST_Amt
                        tmpltDF.at[35, 'Value'] = CGST_Amt
                        tmpltDF.at[36, 'Value'] = SGST_Amt
                        tmpltDF.at[37, 'Value'] = totalAmount
                        tmpltDF.at[100, 'Value'] = arRawDF['Beneficiary Account Number'][index]
                        tmpltDF.at[101, 'Value'] = arRawDF['Beneficiary Bank Name'][index]
                        tmpltDF.at[102, 'Value'] = arRawDF['Bank IFSC'][index]
                        tmpltDF.at[103, 'Value'] = arRawDF['Bank Address'][index]
                        tmpltDF.at[104, 'Value'] = arRawDF['Bank City'][index]
                        tmpltDF.at[105, 'Value'] = arRawDF['Bank Address PIN'][index]
                        tmpltDF.at[106, 'Value'] = arRawDF['Bank State'][index]

                        tmpltDF.at[50, 'Value'] = startDate.strftime("%d-%m-%Y")
                        tmpltDF.at[51, 'Value'] = endDate.strftime("%d-%m-%Y")
                        
                        # Bill No Generation Logic
                        # Step 1: Read the CSV and store it as a dictionary
                        data_dict = {}
                        with open(invoiceNoMaintainerFlPath, 'r') as csv_file:
                            reader = csv.reader(csv_file)
                            for row in reader:
                                if len(row) > 0:
                                    key, value = row[0], row[1]
                                    data_dict[key] = value

                        # Step 2: Modify the invoice no of respective state
                        invoiceYr = data_dict["Year"]
                        if str_date.strftime("%Y") != invoiceYr:
                            for j in data_dict:
                                data_dict[j] = 1
                            data_dict["Year"] = str_date.strftime("%Y")
                            invoiceNo = 1
                        else:
                            for key, value in data_dict.items():
                                if key == arRawDF['Supplier State Alpha Code'][index]:
                                    data_dict[key] = int(data_dict[key]) + 1
                                    invoiceNo = data_dict[key]
                                    break
                                    
                        if int(invoiceNo) < 10:
                            invoiceNo = f"000{invoiceNo}"
                        elif int(invoiceNo) < 100:
                            invoiceNo = f"00{invoiceNo}"
                        elif int(invoiceNo) < 1000:
                            invoiceNo = f"0{invoiceNo}"
                        
                        # Step 3: Write the modified data back to the same CSV file
                        with open(invoiceNoMaintainerFlPath, 'w', newline='') as csv_file:
                            writer = csv.writer(csv_file)
                            for key, value in data_dict.items():
                                writer.writerow([key, value])

                        tmpltDF.at[27, 'Value'] = f"XPL{invoiceYr[len(invoiceYr) - 2:]}{arRawDF['Supplier State Alpha Code'][index]}{invoiceNo}"
                        
                        # Filling Remarks
                        tmpltDF.at[38, 'Value'] = misRawDF['Remarks'][0]
                        tmpltDF.at[39, 'Value'] = misRawDF.shape[0]

                        for index in misRawDF.index:
                            # Filling up Sl. No.
                            tmpltDF.at[40 + index, 'Value'] = 1 + index

                            # Filling up description
                            tmpltDF.at[60 + index, 'Value'] = misRawDF['Short Description'][index]
                            
                            # Filling up Quantity
                            tmpltDF.at[70 + index, 'Value'] = misRawDF['Quantity'][index]

                            # Filling up Rate
                            tmpltDF.at[80 + index, 'Value'] = misRawDF['Rate'][index]

                            # Filling up Amount
                            tmpltDF.at[90 + index, 'Value'] = misRawDF['Amount'][index]
                        
                        tmpltDF.to_excel(f"{formatDataOutPutPath}\Format_Data_{i}.xlsx", index=False)
                        
                        i += 1

def map_log(formatDataOutPutPath):
    
    # Initializing DF list
    df_list = []
    for file in glob.glob(formatDataOutPutPath):
        formatDF = pd.read_excel(file, sheet_name=0, index_col=None)

        # Transpose the DataFrame and reset index
        df = formatDF.T.reset_index(drop=True)
        
        # Rename the columns
        df.columns = df.iloc[0]

        # Drop the first row
        df = df[1:]
        df_list.append(df)

    resultDF = pd.concat(df_list, ignore_index=True)

    return resultDF
